CHIC-Experiment installation
===========================

The original installation instruction can be found at http://wiki.myexperiment.org/index.php/Developer:UbuntuInstallation.
The original one is out-dated that by following that the installation will not be successful. The original automatic installation script
also does not work properly. The following installation notes aim to provide an extended and more detailed instruction. For this installation, Ubuntu 12.03 is tested.

##1 Install dependicies

```
sudo apt-get update

sudo -n apt-get install -y build-essential exim4 git-core curl libcurl3 libcurl3-gnutls libcurl4-gnutls-dev openssl libreadline6 libreadline6-dev zlib1g \
 zlib1g-dev libssl-dev libyaml-dev mysql-server libmysqlclient-dev libxml2-dev libxslt-dev autoconf libc6-dev ncurses-dev automake libtool bison \
 git libmagickwand-dev graphviz gcj-jre-headless
```

Note: While running the following commands you will be prompted to enter a password for the MySQL root user several times. The password will be used to fill the database.yml in following step.

##2 Install RVM and Ruby

Note: Before the installation, public key the repository needs to be install

```
gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3

curl -L https://get.rvm.io | bash -s stable --ruby=1.9.3-p327
```

Then start RVM and Ruby:
```
source ~/.rvm/scripts/rvm
```

##3 Checkout CHIC-experiment from source control
```
git clone https://bitbucket.org/me1kd/chic_experiment.git
```

##3 Install gems
Note: The shell needs to be enable as a login shell
```
/bin/bash --login
```

```
cd CHIC-Experiment
gem update --system 1.8.25
gem install bundler
bundle install
```

##4 Configuration
Note: The password set up previously needs to be filled to database.yml for all three places.

First, you need to create the database config file from the template by running the following command:
```
cp config/database.yml.pre config/database.yml
```
This file contains the database names and log-in credentials for your myExperiment MySQL databases.
Other myExperiment settings can be found in config/default_settings.yml, however you should not modify this file.
Instead, create a config/settings.yml file and any settings you define in there will override the defaults.
```
touch config/settings.yml
```

##5 Set up databases
Create databases
```
bundle exec rake db:create:all
```
Create table structure
```
bundle exec rake db:migrate
```

##6 Enable search
To start Solr, which powers myExperiment's search feature, run the following command:
```bundle exec rake sunspot:solr:start```
and to stop:
```bundle exec rake sunspot:solr:stop```

##7 Run the server

Run the server
```
script/server
```
Then navigate to http://localhost:3000

## Note
There is document for the settings of myexperiemtn: http://wiki.myexperiment.org/index.php/Developer:WindowsInstallation. It also includes the content about how to set up STMP server.